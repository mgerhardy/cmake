CMAKE_ECLIPSE_RESOURCE_ENCODING
---------------------

This cache variable is used by the Eclipse project generator.  See
:manual:`cmake-generators(7)`.

When using the Eclipse project generator, CMake will set the resource
encoding to the given value. If no value is given, no encoding will be
set.
